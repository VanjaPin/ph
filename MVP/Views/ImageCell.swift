//
//  ImageCell.swift
//
//  Created by VanjaPin on 25.05.2020.
//  Copyright © 2020 VanjaPin. All rights reserved.
//
import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
}
