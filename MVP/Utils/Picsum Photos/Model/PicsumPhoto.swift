//
//  PicsumPhoto.swift
//
//  Created by VanjaPin on 25.05.2020.
//  Copyright © 2020 VanjaPin. All rights reserved.
//

struct PicsumPhoto: Codable {
    let id: String
    let author: String
    let originalURL: String
    let width: Int
    let height: Int
    
    enum CodingKeys: String, CodingKey {
        case id
        case author
        case originalURL = "download_url"
        case width
        case height
    }
    
}

//MARK: - Get URL of resized image 
extension PicsumPhoto {
    
    func getResizedImageURL(width: Int, height: Int) -> String {
        guard width > 0, height > 0 else {
            return self.originalURL
        }
        
        return self.originalURL
            .replacingOccurrences(of: String(self.width), with: String(width))
            .replacingOccurrences(of: String(self.height), with: String(height))
    }
    
}
