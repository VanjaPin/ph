//
//  PatternsTableViewController.swift
//
//  Created by VanjaPin on 25.05.2020.
//  Copyright © 2020 VanjaPin. All rights reserved.
//

import UIKit

final class PatternsTableViewController: UITableViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     if let mvpViewController = segue.destination as? MVPViewController {
            let presenter = ConcretePresenter(view: mvpViewController)
            mvpViewController.presenter = presenter
        }

    }

}
